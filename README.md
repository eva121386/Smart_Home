# 基于STM32的智能家居系统

#### 介绍
基于STM32的智能家居系统

#### 软件架构

适配STM32F429IGT6单片机开发板

基于STM32F4xx固件库开发

使用Keil5编写C语言代码

使用Qt编写上位机代码。

完成功能：
1、OLED显示当前温湿度 
2、使用LD3320语音识别模块识别开关灯、电视、冰箱、空调等指令
3、以OLED显示指令文字，LED亮灭表示开关
4、Windows系统下的上位机通过串口1进行控制操作


BSP驱动编写部分：
LED、按键、定时器、USART1串口（用于调试）、USART3（用于语音指令输入）、I2C、DHT11温湿度传感器、OLED显示屏

要点:
USART1和USART3使用中断接收、查询发送的方式进行通信。


以下为实物图:
1、显示温湿度
![显示温湿度](https://foruda.gitee.com/images/1709904043717153012/a574015d_7642578.jpeg "1.jpg")

2、整体图
![整体图](https://foruda.gitee.com/images/1709904086574806354/ed03b69b_7642578.jpeg "2.jpg")

3、一级语言指令响应
![一级语言指令响应](https://foruda.gitee.com/images/1709904150350892328/90250f6a_7642578.jpeg "3.jpg")

二级指令响应
4、开灯
![开灯](https://foruda.gitee.com/images/1709904199791864673/5ac3d944_7642578.jpeg "4.jpg")

5、关灯
![关灯](https://foruda.gitee.com/images/1709904230571106192/22244f49_7642578.jpeg "5.jpg")

6、全部打开
![全部打开](https://foruda.gitee.com/images/1709904287169078367/95fae14c_7642578.jpeg "6.jpg")

7、全部关闭
![全部关闭](https://foruda.gitee.com/images/1709904319939382096/9021bd7d_7642578.jpeg "7.jpg")

8、上位机界面
![上位机界面](Qt%E4%B8%8A%E4%BD%8D%E6%9C%BA/images/1.png)

9、上位机通信
![上位机通信](Qt%E4%B8%8A%E4%BD%8D%E6%9C%BA/images/2.png)