#ifndef __OLED_FONT_H_
#define __OLED_FONT_H_

#include "stm32f4xx.h"
//常用ASCII表
//偏移量32
//ASCII字符集
//偏移量32
//大小:12*6
/************************************6*8的点阵************************************/
extern const unsigned char F6x8[][6];
/****************************************8*16的点阵************************************/
extern const unsigned char F8X16[];
extern char Hzk[][32];

#endif /*__OLED_FONT_H_*/
