#include "bsp.h"			/* 底层硬件驱动 */
#include "bsp_usart1.h"
#include "bsp_usart3.h"
#include "bsp_dht11.h"
#include "bsp_i2c_gpio.h"
#include "bsp_oled.h"

int main(void)
{
 
	uint8_t ucKeyCode;
	bsp_Init();		/* 硬件初始化 */
	USART1_InitConfig(); //USART1 串口初始化
	USART3_InitConfig(); //Voice语言指令输入
	bsp_InitDHT11(); //DHT11温湿度传感器初始化
	bsp_InitI2C(); //I2C初始化
	OLED_Init(); //OLED初始化
	
	DisplayTH(); //OLED 显示温湿度
	while(1)
  {
		ucKeyCode = bsp_GetKey(); //获取按钮输入
		if (ucKeyCode > 0)
		{
			switch (ucKeyCode)
			{
				case KEY_DOWN_K1:		
					DisplayTH();
					break;
				default:
					break;
			}
		}
	}
}





















/*
key-led test
uint8_t ucKeyCode;
ucKeyCode = bsp_GetKey();
if (ucKeyCode > 0)
{
	switch (ucKeyCode)
	{
		case KEY_DOWN_K1:		
		bsp_LedOn(1);	
		printf("K1 DOWN\r\n");
		break;

		case KEY_UP_K1:		
		bsp_LedOff(1);	
		printf("K1 UP\r\n");
		break;				

		case KEY_DOWN_K2:		
		bsp_LedOn(2);	
		printf("K2 DOWN\r\n");
		break;

		case KEY_UP_K2:		
		bsp_LedOff(2);	
		printf("K2 UP\r\n");
		break;	

		case KEY_DOWN_K3:		
		bsp_LedOn(3);	
		printf("K3 DOWN\r\n");
		break;

		case KEY_UP_K3:		
		bsp_LedOff(3);	
		printf("K3 UP\r\n");
		break;

		case KEY_DOWN_K4:		
		bsp_LedOn(4);
		printf("K4 DOWN\r\n");
		break;

		case KEY_UP_K4:		
		bsp_LedOff(4);	
		printf("K4 UP\r\n");
		break;	
	}
}

*/



