#include "bsp_usart3.h"
#include "bsp_oled.h"

//串口3 中断接收 查询发送 Voice语言指令输入

void USART3_InitConfig(void)
{
		GPIO_InitTypeDef GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;

		/* 第1步： 配置GPIO */
    /* 打开 GPIO 时钟 */
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
    /* 打开 UART 时钟 */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3,ENABLE);
    /* 将 PA9 映射为 USART3_TX */
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource10, GPIO_AF_USART3) ;
    
		/* 将 PA10 映射为 USART3_RX */
		GPIO_PinAFConfig(GPIOB, GPIO_PinSource11, GPIO_AF_USART3);
    
    
    /* 配置 USART Tx 为复用功能 */
    GPIO_InitStructure.GPIO_Mode =  GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_OType =  GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_10;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    
    GPIO_Init(GPIOB,&GPIO_InitStructure);
    /* 配置 USART Rx 为复用功能 */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
	
		GPIO_Init(GPIOB, &GPIO_InitStructure);
    
    /* 第2步： 配置串口硬件参数 */
    USART_InitStructure.USART_BaudRate = 9600;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None; 
    USART_InitStructure.USART_Mode = USART_Mode_Rx|USART_Mode_Tx;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    
    USART_Init(USART3,&USART_InitStructure);
    /*配置串口中断*/
    
    NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init( &NVIC_InitStructure);
    
    /*使能串口*/
    USART_Cmd(USART3,ENABLE);
		
		USART_ITConfig(USART3,USART_IT_RXNE,ENABLE);
		//USART_ITConfig(USART3,USART_IT_TXE,ENABLE);
    
    /*清楚标志，防止出现bug*/ 
    USART_ClearFlag(USART3,USART_FLAG_TC);
}

void USART3_IRQHandler(void)
{
	 uint8_t ucTemp;
	 if(USART_GetFlagStatus(USART3, USART_FLAG_RXNE)!=RESET)
	 {
			 ucTemp = USART_ReceiveData(USART3);
		   USART_SendData(USART3,ucTemp);
			 while (USART_GetFlagStatus(USART3, USART_FLAG_TC) == RESET){}
			 switch(ucTemp){
				 case 0:
					 OLED_Clear();
					 OLED_ShowString(0,0,(unsigned char*)"HELLO",16);
					 break;
				 case 1:
					 OLED_Clear();
					 OLED_ShowString(0,0,(unsigned char*)"HELLO!Im Xiao Ai",16);
					 break;
				 case	2:
					 OLED_Clear();
					 OLED_ShowString(0,0,(unsigned char*)"Lamp ON",16);
					 bsp_LedOn(1);
					 break;
				 case 3:
					 OLED_Clear();
					 OLED_ShowString(0,0,(unsigned char*)"Lamp OFF",16);
					 bsp_LedOff(1);
					 break;
				 case 4:
					 OLED_Clear();
					 OLED_ShowString(0,0,(unsigned char*)"TV ON",16);
					 bsp_LedOn(2);
					 break;
				 case 5:
					 OLED_Clear();
					 OLED_ShowString(0,0,(unsigned char*)"TV OFF",16);
					 bsp_LedOff(2);
					 break;
				 case 6:
					 OLED_Clear();
					 OLED_ShowString(0,0,(unsigned char*)"IceBox ON",16);
					 bsp_LedOn(3);
					 break;
				 case 7:
					 OLED_Clear();
					 OLED_ShowString(0,0,(unsigned char*)"IceBox OFF",16);
					 bsp_LedOff(3);
					 break;
				 case 8:
					 OLED_Clear();
					 OLED_ShowString(0,0,(unsigned char*)"Air Conditioning ON",16);
					 bsp_LedOn(4);
					 break;
				 case 9:
					 OLED_Clear();
					 OLED_ShowString(0,0,(unsigned char*)"Air Conditioning OFF",16);
					 bsp_LedOff(4);
					 break;
				 case 10:
					 OLED_Clear();
					 OLED_ShowString(0,0,(unsigned char*)"ALL ON",16);
					 bsp_LedOn(1);
				   bsp_LedOn(2);
				   bsp_LedOn(3);
				   bsp_LedOn(4);
					 break;
				 case 11:
					 OLED_Clear();
					 OLED_ShowString(0,0,(unsigned char*)"ALL OFF",16);
					 bsp_LedOff(1);
				   bsp_LedOff(2);
				   bsp_LedOff(3);
				   bsp_LedOff(4);
					 break;
				 default:
					 OLED_Clear();
					 OLED_ShowString(0,0,(unsigned char*)"Unknown Commend",16);
					 break;
			 }
	 }
}


