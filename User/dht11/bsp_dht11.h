#ifndef __BSP_DHT11_H_
#define __BSP_DHT11_H_

#include "bsp.h"

/* 定义GPIO端口 */
#define RCC_DQ		RCC_AHB1Periph_GPIOE
#define PORT_DQ		GPIOE
#define PIN_DQ		GPIO_Pin_2

#define DQ_0()		PORT_DQ->BSRRH = PIN_DQ
#define DQ_1()		PORT_DQ->BSRRL = PIN_DQ

/* 判断DQ输入是否为低 */
#define DQ_IS_LOW()	((PORT_DQ->IDR & PIN_DQ) == 0)

typedef struct
{
	uint8_t Buf[5];
	uint8_t Temp;		/* Temperature 温度 摄氏度 */
	uint8_t Hum;		/* Humidity 湿度 百分比 */
}DHT11_T;

extern char temp[20];
extern char hum[20];
extern DHT11_T tDHT;

void bsp_InitDHT11(void);
uint8_t DHT11_ReadData(DHT11_T *_pDTH);
void DisplayTH(void);
#endif /*__BSP_DHT11_H_*/

