#include "bsp_usart1.h"
#include "AT_com.h"
#include "bsp_oled.h"

//中断接收 ，查询发送

void USART1_InitConfig(void)
{
	
		
    GPIO_InitTypeDef GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;

		/* 第1步： 配置GPIO */
    /* 打开 GPIO 时钟 */
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
    /* 打开 UART 时钟 */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1,ENABLE);
    /* 将 PA9 映射为 USART1_TX */
    GPIO_PinAFConfig(GPIOA,GPIO_PinSource9,GPIO_AF_USART1) ;
    
		/* 将 PA10 映射为 USART1_RX */
		GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_USART1);
    
    
    /* 配置 USART Tx 为复用功能 */
    GPIO_InitStructure.GPIO_Mode =  GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_OType =  GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_9;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    
    GPIO_Init(GPIOA,&GPIO_InitStructure);
    /* 配置 USART Rx 为复用功能 */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	
		GPIO_Init(GPIOA, &GPIO_InitStructure);
    
    /* 第2步： 配置串口硬件参数 */
    USART_InitStructure.USART_BaudRate = 115200;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None; 
    USART_InitStructure.USART_Mode = USART_Mode_Rx|USART_Mode_Tx;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    
    USART_Init(USART1,&USART_InitStructure);
    /*配置串口中断*/
    
    NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init( &NVIC_InitStructure);
    
    USART_ITConfig(USART1,USART_IT_RXNE,ENABLE);
		//USART_ITConfig(USART1,USART_IT_TXE,ENABLE);
		
		/*使能串口*/
    USART_Cmd(USART1,ENABLE);
    
    /*清楚标志，防止出现bug*/ 
    USART_ClearFlag(USART1,USART_FLAG_TC);
    
}

int fputc(int ch, FILE *f)
{
    USART_SendData(USART1, (uint8_t) ch);
 
   /* Loop until the end of transmission */
   while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET)
  {}
 
   return ch;

}

void USART1_IRQHandler(void)
{
    uint8_t ucTemp;
		if(USART_GetITStatus(USART1 ,USART_IT_RXNE) == SET)  //接收数据寄存器不空
    {
        ucTemp = USART_ReceiveData(USART1);
//				USART_SendData(USART1, ucTemp);
//				while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);
				switch(ucTemp){
				 case 0:
					 OLED_Clear();
					 OLED_ShowString(0,0,(unsigned char*)"HELLO",16);
					 break;
				 case '0':
					 OLED_Clear();
					 OLED_ShowString(0,0,(unsigned char*)"USART1 TEST",16);
					 printf("AT");
					 break;
				 case	'1':
					 OLED_Clear();
					 OLED_ShowString(0,0,(unsigned char*)"Lamp ON",16);
					 printf(AT_LAMPON);
					 bsp_LedOn(1);
					 break;
				 case '2':
					 OLED_Clear();
					 OLED_ShowString(0,0,(unsigned char*)"Lamp OFF",16);
					 bsp_LedOff(1);
					 printf(AT_LAMPOFF);
					 break;
				 case '3':
					 OLED_Clear();
					 OLED_ShowString(0,0,(unsigned char*)"TV ON",16);
					 bsp_LedOn(2);
					 printf(AT_TVON);
					 break;
				 case '4':
					 OLED_Clear();
					 OLED_ShowString(0,0,(unsigned char*)"TV OFF",16);
					 bsp_LedOff(2);
					 printf(AT_TVOFF);
					 break;
				 case '5':
					 OLED_Clear();
					 OLED_ShowString(0,0,(unsigned char*)"IceBox ON",16);
					 bsp_LedOn(3);
					 printf(AT_ICEON);
					 break;
				 case '6':
					 OLED_Clear();
					 OLED_ShowString(0,0,(unsigned char*)"IceBox OFF",16);
					 bsp_LedOff(3);
					 printf(AT_ICEOFF);
					 break;
				 case '7':
					 OLED_Clear();
					 OLED_ShowString(0,0,(unsigned char*)"Air Conditioning ON",16);
					 bsp_LedOn(4);
					 printf(AT_AIRON);
					 break;
				 case '8':
					 OLED_Clear();
					 OLED_ShowString(0,0,(unsigned char*)"Air Conditioning OFF",16);
					 bsp_LedOff(4);
					 printf(AT_AIROFF);
					 break;
				 case 10:
					 OLED_Clear();
					 OLED_ShowString(0,0,(unsigned char*)"ALL ON",16);
					 bsp_LedOn(1);
				   bsp_LedOn(2);
				   bsp_LedOn(3);
				   bsp_LedOn(4);
					 break;
				 case 11:
					 OLED_Clear();
					 OLED_ShowString(0,0,(unsigned char*)"ALL OFF",16);
					 bsp_LedOff(1);
				   bsp_LedOff(2);
				   bsp_LedOff(3);
				   bsp_LedOff(4);
					 break;
				 default:
					 OLED_Clear();
					 OLED_ShowString(0,0,(unsigned char*)"Unknown Commend",16);
					 break;
			 }
    }
//    else if(USART_GetITStatus(USART1,USART_IT_TXE) == SET) //发送寄存器空
//    {
//        if(ucTxindex < BUFFSIZE)
//        {
//            USART_SendData(USART1,aTxBuff[ucTxindex++]);
//        }
//        
//        else
//        {
//            USART_ITConfig(USART1,USART_IT_TXE,DISABLE);
//        }
//        
//    }
    
}

