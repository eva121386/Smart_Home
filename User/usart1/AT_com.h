#ifndef __AT_COM_H_
#define __AT_COM_H_


#define AT	"AT\n"
#define AT_ASK	"OK\n"
#define AT_ERROR	"ERROR\n"

#define AT_LAMPON		"AT+CTRL=<LAMP>,<ON>\n"
#define AT_LAMPOFF	"AT+CTRL=<LAMP>,<OFF>\n"
#define AT_TVON			"AT+CTRL=<TV>,<ON>\n"
#define AT_TVOFF		"AT+CTRL=<TV>,<OFF>\n"
#define AT_ICEON		"AT+CTRL=<ICE>,<ON>\n"
#define AT_ICEOFF		"AT+CTRL=<ICE>,<OFF>\n"
#define AT_AIRON		"AT+CTRL=<AIR>,<ON>\n"
#define AT_AIROFF		"AT+CTRL=<AIR>,<OFF>\n"

#endif
