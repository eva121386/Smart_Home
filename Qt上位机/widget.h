#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

#include <QSerialPort>        //提供访问串口的功能
#include <QSerialPortInfo>    //提供系统中存在的串口的信息

QT_BEGIN_NAMESPACE
namespace Ui {
class Widget;
}
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

public slots:
    void OpenPort();
    void ReadData();
    void LampOn();
    void LampOff();
    void TVOn();
    void TVOff();
    void IceOn();
    void IceOff();
    void AirOn();
    void AirOff();
private:
    Ui::Widget *ui;
    QSerialPort serial;
    bool IsOpenPort=false;
};
#endif // WIDGET_H
