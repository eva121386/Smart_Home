#include "widget.h"
#include "ui_widget.h"
#include <QMessageBox>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    ui->comboBox->clear();
    //通过QSerialPortInfo查找可用串口
    foreach(const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
    {
        ui->comboBox->addItem(info.portName());
    }
    if(ui->comboBox->count()==0){
        ui->OpenPortBt->setEnabled(false);
    }
    connect(ui->OpenPortBt,&QPushButton::clicked,this,&Widget::OpenPort);
    connect(&serial,&QSerialPort::readyRead,this,&Widget::ReadData);
    connect(ui->LampONBt,&QPushButton::clicked,this,&Widget::LampOn);
    connect(ui->LampOFFBt,&QPushButton::clicked,this,&Widget::LampOff);
    connect(ui->TVONBt,&QPushButton::clicked,this,&Widget::TVOn);
    connect(ui->TVOFFBt,&QPushButton::clicked,this,&Widget::TVOff);
    connect(ui->IceboxONBt,&QPushButton::clicked,this,&Widget::IceOn);
    connect(ui->IceboxOFFBt,&QPushButton::clicked,this,&Widget::IceOff);
    connect(ui->AirConOnBt,&QPushButton::clicked,this,&Widget::AirOn);
    connect(ui->AirConOFFBt,&QPushButton::clicked,this,&Widget::AirOff);
}

Widget::~Widget()
{
    serial.close();
    delete ui;
}

void Widget::OpenPort()
{
    if(!IsOpenPort){

        //设置串口名
        serial.setPortName(ui->comboBox->currentText());
        //设置波特率
        serial.setBaudRate(QSerialPort::Baud115200);
        //设置数据位数
        serial.setDataBits(QSerialPort::Data8);
        //设置奇偶校验
        serial.setParity(QSerialPort::NoParity);
        //设置停止位
        serial.setStopBits(QSerialPort::OneStop);
        //设置流控制
        serial.setFlowControl(QSerialPort::NoFlowControl);

        //打开串口
        serial.open(QIODevice::ReadWrite);

        if(!serial.isOpen()){
            QMessageBox::warning(this,"警告","串口打开失败");
        }else{
            serial.write("0");
            ui->OpenPortBt->setText("关闭串口");
            ui->comboBox->setEnabled(false);
        }
        IsOpenPort=true;
    }else{
        serial.close();
        ui->OpenPortBt->setText("打开串口");
        ui->comboBox->clear();
        //通过QSerialPortInfo查找可用串口
        foreach(const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
        {
            ui->comboBox->addItem(info.portName());
        }
        ui->comboBox->setEnabled(true);
        IsOpenPort=false;
    }
}

void Widget::ReadData()
{
    //从接收缓冲区中读取数据
    QByteArray buffer = serial.readAll();
    QString data=QString(buffer);
    ui->ComListWidget->addItem(data);
}

void Widget::LampOn()
{
    serial.write("1");
}

void Widget::LampOff()
{
    serial.write("2");
}

void Widget::TVOn()
{
    serial.write("3");
}

void Widget::TVOff()
{
    serial.write("4");
}

void Widget::IceOn()
{
    serial.write("5");
}

void Widget::IceOff()
{
    serial.write("6");
}

void Widget::AirOn()
{
    serial.write("7");
}

void Widget::AirOff()
{
    serial.write("8");
}
